# unknownNG Beta 4

My 2D game engine written in C++ and SDL2  

Written in C++  
SDL2 available [here](https://libsdl.org/index.php)  

## Build and run

Dependencies (on Linux): `libsdl2-dev`, `libsdl2-image-dev`  

1. `make`
2. `./test`

## Licenses

### unknownNG Beta 4 license (GPLv3)

My 2D game engine written in C++ and SDL2  
Copyright (C) 2022  TheNieznany \<thenieznany11 at protonmail dot com\>  

This program is free software: you can redistribute it and/or modify  
it under the terms of the GNU General Public License as published by  
the Free Software Foundation, either version 3 of the License, or  
(at your option) any later version.  

This program is distributed in the hope that it will be useful,  
but WITHOUT ANY WARRANTY; without even the implied warranty of  
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License  
along with this program.  If not, see <https://www.gnu.org/licenses/>.  

<br>
<br>
<br>

### SDL2 license (zlib)

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.

