/*
 * My 2D game engine written in C++ and SDL2
 * Copyright (C) 2022  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This file is part of unknownNG Beta 4.
 *
 * unknownNG Beta 4 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * unknownNG Beta 4 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with unknownNG Beta 4.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "core/rect.hpp"
#include "core/utility.hpp"
#include "core/vector2.hpp"

namespace Color
{
	const SDL_Color BLACK	= {0,   0,   0,   255};
	const SDL_Color RED		= {240, 0,   0,   255};
	const SDL_Color GREEN	= {0,   240, 0,   255};
	const SDL_Color BLUE	= {0,   0,   240, 255};
	const SDL_Color WHITE	= {240, 240, 240, 255};
};

class Graphics
{
public:
	SDL_Renderer* renderer;

	double scale;

	double width;
	double height;

	double tileSize;



	Graphics(int a_scale, const char* title, int a_width, int a_height);
	~Graphics();



	void clear();
	void render();

	void setColor(const SDL_Color& color);

	void drawRect(const Rect& rect);
	void fillRect(const Rect& rect);

	void drawLine(const Vector2& p1, const Vector2& p2);

	void drawPoint(const Vector2& p);

private:
	SDL_Window* window;

	void createWindow(const char* title, int width, int height);
	void createRenderer();
};
