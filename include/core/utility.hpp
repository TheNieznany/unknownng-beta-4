/*
 * My 2D game engine written in C++ and SDL2
 * Copyright (C) 2022  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This file is part of unknownNG Beta 4.
 *
 * unknownNG Beta 4 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * unknownNG Beta 4 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with unknownNG Beta 4.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <cstdarg>
#include <cstdio>
#include <cstdlib>
#include <ctime>

void quit();

double clamp(double value, double min, double max);

bool inRange(double value, double from, double to);

double random_d();
double random_d(double min, double max);

int random_i(int min, int max);



void log(const char* type, const char* format, ...);

#define LOG_INFO(...) log("\x1b[1;34m[INFO]\x1b[0m", __VA_ARGS__)
#define LOG_WARN(...) log("\x1b[1;33m[WARN]\x1b[0m", __VA_ARGS__)
#define LOG_ERROR(...) log("\x1b[1;31m[ERROR]\x1b[0m", __VA_ARGS__)

#define ASSERT(expr, ...) do	\
{								\
	if ((expr) == false)		\
	{							\
		LOG_ERROR(__VA_ARGS__); \
		quit();					\
	}							\
} while(false)
