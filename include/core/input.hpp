/*
 * My 2D game engine written in C++ and SDL2
 * Copyright (C) 2022  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This file is part of unknownNG Beta 4.
 *
 * unknownNG Beta 4 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * unknownNG Beta 4 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with unknownNG Beta 4.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <SDL2/SDL.h>

#include <cstdint>
#include <map>

#include "core/graphics.hpp"
#include "core/utility.hpp"
#include "core/vector2.hpp"

class Input
{
public:
	Input(Graphics& g);



	bool update();

	bool isKeyDown(SDL_Keycode key);
	bool isKeyUp(SDL_Keycode key);
	bool isKeyHeld(SDL_Keycode key);

	bool isButtonDown(uint8_t button);
	bool isButtonUp(uint8_t button);
	bool isButtonHeld(uint8_t button);

	Vector2 getMousePos();

private:
	Graphics& graphics;
	Vector2 mousePos;

	SDL_Event event;

	std::map<SDL_Keycode, bool> keysDown;
	std::map<SDL_Keycode, bool> keysUp;
	std::map<SDL_Keycode, bool> keysHeld;

	std::map<uint8_t, bool> buttonsDown;
	std::map<uint8_t, bool> buttonsUp;
	std::map<uint8_t, bool> buttonsHeld;
};
