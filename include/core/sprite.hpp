/*
 * My 2D game engine written in C++ and SDL2
 * Copyright (C) 2022  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This file is part of unknownNG Beta 4.
 *
 * unknownNG Beta 4 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * unknownNG Beta 4 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with unknownNG Beta 4.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <map>
#include <string>
#include <vector>

#include "core/rect.hpp"
#include "core/texture.hpp"
#include "core/utility.hpp"
#include "core/vector2.hpp"

struct Animation
{
	std::vector<Rect> rects;
	size_t frameAmount;
	double frameDuration;

	Animation(){}; // needed because modern C++ (?)

	Animation(const Rect& initialRect, size_t fa, double fd)
	{
		rects.clear();

		ASSERT(fa != 0, "Sprite: Animation frame amount should not be zero!");
		ASSERT(fd > 0, "Sprite: Animation frame duration should be positive!");

		for (size_t i = 0; i < fa; i++)
		{
			Rect tmp = {
				initialRect.x + (initialRect.w * static_cast<double>(i)),
				initialRect.y,
				initialRect.w,
				initialRect.h
			};
			rects.push_back(tmp);
		}

		frameAmount = fa;
		frameDuration = fd;
	}
};

class Sprite
{
public:
	Texture& texture;
	std::map<std::string, Animation> animations;

	std::string currentAnim;
	size_t currentFrame;



	Sprite(Texture& t);



	void update(double elapsedTime);
	void draw(const Vector2& point);

	void addAnimation(const std::string& name, const Animation& anim);
	void playAnimation(const std::string& name);
};
