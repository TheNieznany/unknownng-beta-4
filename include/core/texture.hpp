/*
 * My 2D game engine written in C++ and SDL2
 * Copyright (C) 2022  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This file is part of unknownNG Beta 4.
 *
 * unknownNG Beta 4 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * unknownNG Beta 4 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with unknownNG Beta 4.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "core/graphics.hpp"
#include "core/rect.hpp"
#include "core/utility.hpp"
#include "core/vector2.hpp"

class Texture
{
public:
	SDL_Texture* data;

	double width;
	double height;



	Texture(Graphics& g, const char* filepath);
	~Texture();



	void draw(const Vector2& point);
	void draw(const Rect& srcRect, const Vector2& point);

private:
	Graphics& graphics;
};
