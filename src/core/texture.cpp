/*
 * My 2D game engine written in C++ and SDL2
 * Copyright (C) 2022  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This file is part of unknownNG Beta 4.
 *
 * unknownNG Beta 4 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * unknownNG Beta 4 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with unknownNG Beta 4.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "core/texture.hpp"

Texture::Texture(Graphics& g, const char* filepath):
	graphics(g)
{
	ASSERT(filepath != nullptr, "Texture: Invalid pointer to filepath string!");

	SDL_Surface* sur = IMG_Load(filepath);
	ASSERT(
		sur != nullptr,
		"Texture: IMG_Load failed. %s", IMG_GetError()
	);

	width = static_cast<double>(sur->w);
	height = static_cast<double>(sur->h);

	data = SDL_CreateTextureFromSurface(graphics.renderer, sur);
	ASSERT(
		data != nullptr,
		"Texture: SDL_CreateTextureFromSurface failed. %s", SDL_GetError()
	);

	LOG_INFO("Texture: Loaded %s as texture of size %dx%d", filepath, sur->w, sur->h);
	SDL_FreeSurface(sur);
}

Texture::~Texture()
{
	SDL_DestroyTexture(data);
}





void Texture::draw(const Vector2& point)
{
	SDL_Rect dst = {
		static_cast<int>(point.x * graphics.scale),
		static_cast<int>(point.y * graphics.scale),
		static_cast<int>(width * graphics.scale),
		static_cast<int>(height * graphics.scale)
	};

	SDL_RenderCopy(graphics.renderer, data, nullptr, &dst);
}

void Texture::draw(const Rect& srcRect, const Vector2& point)
{
	SDL_Rect src = {
		static_cast<int>(srcRect.x),
		static_cast<int>(srcRect.y),
		static_cast<int>(srcRect.w),
		static_cast<int>(srcRect.h)
	};

	SDL_Rect dst = {
		static_cast<int>(point.x * graphics.scale),
		static_cast<int>(point.y * graphics.scale),
		static_cast<int>(srcRect.w * graphics.scale),
		static_cast<int>(srcRect.h * graphics.scale)
	};

	SDL_RenderCopy(graphics.renderer, data, &src, &dst);
}
