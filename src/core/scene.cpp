/*
 * My 2D game engine written in C++ and SDL2
 * Copyright (C) 2022  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This file is part of unknownNG Beta 4.
 *
 * unknownNG Beta 4 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * unknownNG Beta 4 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with unknownNG Beta 4.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "core/scene.hpp"

Scene::Scene(Graphics& g, Input& i):
	graphics(g),
	input(i)
{
	running = true;
}

Scene::~Scene(){} // because its virtual

void Scene::run()
{
	uint32_t elapsedTime = 1;

	while (running)
	{
		uint32_t frameStart = SDL_GetTicks();

		base_handleInput();
		base_update(elapsedTime);
		base_draw();

		elapsedTime = SDL_GetTicks() - frameStart;
	}
}





void Scene::base_handleInput()
{
	if (input.update()) running = false;
	handleInput();
}

void Scene::base_update(uint32_t elapsedTime)
{
	double et = static_cast<double>(elapsedTime) / 1000.0;
	update(et);
}

void Scene::base_draw()
{
	graphics.clear();
	draw();
	graphics.render();
}
