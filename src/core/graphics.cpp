/*
 * My 2D game engine written in C++ and SDL2
 * Copyright (C) 2022  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This file is part of unknownNG Beta 4.
 *
 * unknownNG Beta 4 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * unknownNG Beta 4 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with unknownNG Beta 4.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "core/graphics.hpp"

Graphics::Graphics(int a_scale, const char* title, int a_width, int a_height)
{
	ASSERT(a_scale > 0, "Graphics: Scale should be positive!");

	scale = static_cast<double>(a_scale);

	ASSERT(
		(a_width > 0) || (a_height > 0),
		"Graphics: Window size should be positive!"
	);

	width = static_cast<double>(a_width);
	height = static_cast<double>(a_height);

	ASSERT(
		SDL_WasInit(SDL_INIT_VIDEO) != 0,
		"Graphics: SDL_Init needs SDL_INIT_VIDEO flag!"
	);

	createWindow(title, a_width * a_scale, a_height * a_scale);
	createRenderer();
}

Graphics::~Graphics()
{
	SDL_DestroyRenderer(renderer);
	LOG_INFO("Graphics: Destroyed renderer");

	SDL_DestroyWindow(window);
	LOG_INFO("Graphics: Destroyed window");
}





void Graphics::createWindow(const char* title, int a_width, int a_height)
{
	window = SDL_CreateWindow(
		title,
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		a_width,
		a_height,
		0
	);
	ASSERT(
		window != nullptr,
		"Graphics: SDL_CreateWindow failed. %s", SDL_GetError()
	);

	LOG_INFO("Graphics: Created window of size %dx%d", a_width, a_height);
}

void Graphics::createRenderer()
{
	renderer = SDL_CreateRenderer(
		window,
		-1,
		SDL_RENDERER_ACCELERATED |
		SDL_RENDERER_PRESENTVSYNC
	);
	ASSERT(
		renderer != nullptr,
		"Graphics: SDL_CreateRenderer failed. %s", SDL_GetError()
	);

	LOG_INFO("Graphics: Created renderer");
}





void Graphics::clear()
{
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
	SDL_RenderClear(renderer);
}

void Graphics::render()
{
	SDL_RenderPresent(renderer);
}

void Graphics::setColor(const SDL_Color& color)
{
	SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
}





void Graphics::drawRect(const Rect& rect)
{
	SDL_Rect tmp = {
		static_cast<int>(rect.x * scale),
		static_cast<int>(rect.y * scale),
		static_cast<int>(rect.w * scale),
		static_cast<int>(rect.h * scale)
	};

	SDL_RenderDrawRect(renderer, &tmp);
}

void Graphics::fillRect(const Rect& rect)
{
	SDL_Rect tmp = {
		static_cast<int>(rect.x * scale),
		static_cast<int>(rect.y * scale),
		static_cast<int>(rect.w * scale),
		static_cast<int>(rect.h * scale)
	};

	SDL_RenderFillRect(renderer, &tmp);
}

void Graphics::drawLine(const Vector2& p1, const Vector2& p2)
{
	SDL_RenderDrawLine(
		renderer,
		static_cast<int>(p1.x * scale),
		static_cast<int>(p1.y * scale),
		static_cast<int>(p2.x * scale),
		static_cast<int>(p2.y * scale)
	);
}

void Graphics::drawPoint(const Vector2& p)
{
	SDL_RenderDrawPoint(
		renderer,
		static_cast<int>(p.x * scale),
		static_cast<int>(p.y * scale)
	);
}
