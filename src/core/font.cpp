/*
 * My 2D game engine written in C++ and SDL2
 * Copyright (C) 2022  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This file is part of unknownNG Beta 4.
 *
 * unknownNG Beta 4 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * unknownNG Beta 4 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with unknownNG Beta 4.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "core/font.hpp"

Font::Font(
	Texture& a_texture,
	int a_width,
	int a_height,
	int a_glyphSpacing,
	int a_lineSpacing
): texture(a_texture)
{
	ASSERT(
		(a_width > 0) || (a_height > 0),
		"Font: Dimensions should be positive!"
	);

	width = static_cast<double>(a_width);
	height = static_cast<double>(a_height);

	ASSERT(
		(a_glyphSpacing >= 0) || (a_lineSpacing >= 0),
		"Font: Spacing should be non-negative!"
	);

	glyphSpacing = static_cast<double>(a_glyphSpacing);
	lineSpacing = static_cast<double>(a_lineSpacing);
}

void Font::write(const std::string& text, const Vector2& point)
{
	Vector2 nextGlyph = point;

	for (size_t i = 0; i < text.size(); i++)
	{
		if (text[i] != '\n')
		{
			texture.draw(
				{static_cast<double>(text[i] - 32) * width, 0.0, width, height},
				nextGlyph
			);

			nextGlyph.x += width + glyphSpacing;
		}
		else
		{
			nextGlyph.x = point.x;
			nextGlyph.y += height + lineSpacing;
		}
	}
}
