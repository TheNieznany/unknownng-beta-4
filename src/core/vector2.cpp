/*
 * My 2D game engine written in C++ and SDL2
 * Copyright (C) 2022  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This file is part of unknownNG Beta 4.
 *
 * unknownNG Beta 4 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * unknownNG Beta 4 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with unknownNG Beta 4.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "core/vector2.hpp"

Vector2::Vector2()
{
	x = 0.0;
	y = 0.0;
}

Vector2::Vector2(double a_x, double a_y)
{
	x = a_x;
	y = a_y;
}

Vector2::Vector2(const Vector2& vec)
{
	x = vec.x;
	y = vec.y;
}

Vector2& Vector2::operator=(const Vector2& rhs)
{
	if (this != &rhs)
	{
		x = rhs.x;
		y = rhs.y;
	}

	return *this;
}





Vector2& Vector2::operator+=(const Vector2& rhs)
{
	x += rhs.x;
	y += rhs.y;

	return *this;
}

const Vector2 Vector2::operator+(const Vector2& other) const
{
	Vector2 result = *this;
	result += other;
	return result;
}

Vector2& Vector2::operator-=(const Vector2& rhs)
{
	x -= rhs.x;
	y -= rhs.y;

	return *this;
}

const Vector2 Vector2::operator-(const Vector2& other) const
{
	Vector2 result = *this;
	result -= other;
	return result;
}





Vector2& Vector2::operator*=(double scalar)
{
	x *= scalar;
	y *= scalar;

	return *this;
}

const Vector2 Vector2::operator*(double scalar) const
{
	Vector2 result = *this;
	result *= scalar;
	return result;
}

Vector2& Vector2::operator/=(double scalar)
{
	if (scalar == 0.0)
	{
		x = 0.0;
		y = 0.0;
	}
	else
	{
		if (x != 0.0) x /= scalar;
		if (y != 0.0) y /= scalar;
	}

	return *this;
}

const Vector2 Vector2::operator/(double scalar) const
{
	Vector2 result = *this;
	result /= scalar;
	return result;
}





bool Vector2::operator==(const Vector2& other) const
{
	return (x == other.x) && (y == other.y);
}

bool Vector2::operator!=(const Vector2& other) const
{
	return !(*this == other);
}





double Vector2::angle() const
{
	return atan2(y, x) * 180.0 / acos(-1.0);
}

double Vector2::length() const
{
	return sqrt((x * x) + (y * y));
}





const Vector2 Vector2::directionTo(const Vector2& other) const
{
	Vector2 result = other - *this;
	return result.normalized();
}

const Vector2 Vector2::normalized() const
{
	Vector2 result = *this;

	double len = this->length();

	result.x /= len;
	result.y /= len;

	return result;
}

const Vector2 Vector2::rotatedBy(double degrees) const
{
	const static double DEG_TO_RAD = acos(-1.0) / 180.0;

	double c = cos(degrees * DEG_TO_RAD);
	double s = sin(degrees * DEG_TO_RAD);

	Vector2 result;

	result.x = (c * this->x) - (s * this->y);
	result.y = (s * this->x) + (c * this->y);

	return result;
}
