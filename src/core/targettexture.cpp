/*
 * My 2D game engine written in C++ and SDL2
 * Copyright (C) 2022  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This file is part of unknownNG Beta 4.
 *
 * unknownNG Beta 4 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * unknownNG Beta 4 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with unknownNG Beta 4.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "core/targettexture.hpp"

TargetTexture::TargetTexture(Graphics& g, int w, int h):
	graphics(g)
{
	width = static_cast<double>(w);
	height = static_cast<double>(h);

	data = SDL_CreateTexture(
		graphics.renderer,
		SDL_PIXELFORMAT_UNKNOWN,
		SDL_TEXTUREACCESS_TARGET,
		w,
		h
	);
	ASSERT(
		data != nullptr,
		"TargetTexture: SDL_CreateTexture failed. %s", SDL_GetError()
	);

	ASSERT(
		SDL_SetTextureBlendMode(data, SDL_BLENDMODE_BLEND) == 0,
		"TargetTexture: SDL_SetTextureBlendMode failed. %s", SDL_GetError()
	);

	clearTarget();
	LOG_INFO("TargetTexture: Created target texture of size %dx%d", w, h);
}

TargetTexture::~TargetTexture()
{
	SDL_DestroyTexture(data);
}





void TargetTexture::clearTarget()
{
	SDL_SetRenderTarget(graphics.renderer, data);

	SDL_SetRenderDrawColor(graphics.renderer, 0, 0, 0, 0);
	SDL_RenderClear(graphics.renderer);

	SDL_SetRenderTarget(graphics.renderer, nullptr);
}





void TargetTexture::draw(const Vector2& point)
{
	SDL_Rect dst = {
		static_cast<int>(point.x * graphics.scale),
		static_cast<int>(point.y * graphics.scale),
		static_cast<int>(width * graphics.scale),
		static_cast<int>(height * graphics.scale)
	};

	SDL_RenderCopy(graphics.renderer, data, nullptr, &dst);
}

void TargetTexture::draw(const Rect& srcRect, const Vector2& point)
{
	SDL_Rect src = {
		static_cast<int>(srcRect.x),
		static_cast<int>(srcRect.y),
		static_cast<int>(srcRect.w),
		static_cast<int>(srcRect.h)
	};

	SDL_Rect dst = {
		static_cast<int>(point.x * graphics.scale),
		static_cast<int>(point.y * graphics.scale),
		static_cast<int>(srcRect.w * graphics.scale),
		static_cast<int>(srcRect.h * graphics.scale)
	};

	SDL_RenderCopy(graphics.renderer, data, &src, &dst);
}





void TargetTexture::drawTo(Texture* texture, const Vector2& point)
{
	SDL_Rect dst = {
		static_cast<int>(point.x),
		static_cast<int>(point.y),
		static_cast<int>(texture->width),
		static_cast<int>(texture->height)
	};

	SDL_SetRenderTarget(graphics.renderer, data);
	SDL_RenderCopy(graphics.renderer, texture->data, nullptr, &dst);
	SDL_SetRenderTarget(graphics.renderer, nullptr);
}

void TargetTexture::drawTo(
	Texture* texture,
	const Rect& srcRect,
	const Vector2& point
){
	SDL_Rect src = {
		static_cast<int>(srcRect.x),
		static_cast<int>(srcRect.y),
		static_cast<int>(srcRect.w),
		static_cast<int>(srcRect.h)
	};

	SDL_Rect dst = {
		static_cast<int>(point.x),
		static_cast<int>(point.y),
		static_cast<int>(srcRect.w),
		static_cast<int>(srcRect.h)
	};

	SDL_SetRenderTarget(graphics.renderer, data);
	SDL_RenderCopy(graphics.renderer, texture->data, &src, &dst);
	SDL_SetRenderTarget(graphics.renderer, nullptr);
}
