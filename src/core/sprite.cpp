/*
 * My 2D game engine written in C++ and SDL2
 * Copyright (C) 2022  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This file is part of unknownNG Beta 4.
 *
 * unknownNG Beta 4 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * unknownNG Beta 4 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with unknownNG Beta 4.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "core/sprite.hpp"

Sprite::Sprite(Texture& t):
	texture(t)
{
	animations.clear();
	currentAnim = "";
	currentFrame = 0;
}





void Sprite::update(double elapsedTime)
{
	static double acc = 0.0;
	acc += elapsedTime;

	if (acc >= animations[currentAnim].frameDuration)
	{
		acc -= animations[currentAnim].frameDuration;

		currentFrame++;
		if (currentFrame == animations[currentAnim].frameAmount)
		{
			currentFrame = 0;
		}
	}
}

void Sprite::draw(const Vector2& point)
{
	texture.draw(animations[currentAnim].rects[currentFrame], point);
}





void Sprite::addAnimation(const std::string& name, const Animation& anim)
{
	animations[name] = anim;
}

void Sprite::playAnimation(const std::string& name)
{
	currentAnim = name;
	currentFrame = 0;
}
