/*
 * My 2D game engine written in C++ and SDL2
 * Copyright (C) 2022  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This file is part of unknownNG Beta 4.
 *
 * unknownNG Beta 4 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * unknownNG Beta 4 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with unknownNG Beta 4.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "core/utility.hpp"

static FILE* logFile = stdout;

void quit()
{
	exit(EXIT_FAILURE);
}

double clamp(double value, double min, double max)
{
	if (value < min) return min;
	if (value > max) return max;

	return value;
}

bool inRange(double value, double from, double to)
{
	return (value >= from) && (value <= to);
}

double random_d()
{
	return (static_cast<double>(rand()) / static_cast<double>(RAND_MAX));
}

double random_d(double min, double max)
{
	int min2 = static_cast<int>(min);
	int max2 = static_cast<int>(max);

	return static_cast<double>(rand() % (max2 - min2 + 1) + min2);
}

int random_i(int min, int max)
{
	return rand() % (max - min + 1) + min;
}

void log(const char* type, const char* format, ...)
{
	va_list args;
	va_start(args, format);

	time_t now2 = time(nullptr);
	tm* now = localtime(&now2);

	char timestamp[10];
	strftime(timestamp, 10, "%H:%M:%S", now);

	fprintf(logFile, "%s %s ", timestamp, type);
	vfprintf(logFile, format, args);
	fputc('\n', logFile);

	va_end(args);
}
