/*
 * My 2D game engine written in C++ and SDL2
 * Copyright (C) 2022  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This file is part of unknownNG Beta 4.
 *
 * unknownNG Beta 4 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * unknownNG Beta 4 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with unknownNG Beta 4.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "core/input.hpp"

Input::Input(Graphics& g):
	graphics(g)
{
	mousePos = {0.0, 0.0};

	keysHeld.clear();
	buttonsHeld.clear();
}

bool Input::update()
{
	keysDown.clear();
	keysUp.clear();

	buttonsDown.clear();
	buttonsUp.clear();

	while (SDL_PollEvent(&event) != 0)
	{
		switch (event.type)
		{
		case SDL_QUIT:
			return true;

		case SDL_KEYDOWN:
			if (event.key.repeat == 0)
			{
				keysDown[event.key.keysym.sym] = true;
				keysHeld[event.key.keysym.sym] = true;
			}
			break;

		case SDL_KEYUP:
			keysUp[event.key.keysym.sym] = true;
			keysHeld[event.key.keysym.sym] = false;
			break;

		case SDL_MOUSEMOTION:
			mousePos.x = static_cast<double>(event.motion.x);
			mousePos.y = static_cast<double>(event.motion.y);
			mousePos /= graphics.scale;
			break;

		case SDL_MOUSEBUTTONDOWN:
			buttonsDown[event.button.button] = true;
			buttonsHeld[event.button.button] = true;
			break;

		case SDL_MOUSEBUTTONUP:
			buttonsUp[event.button.button] = true;
			buttonsHeld[event.button.button] = false;
			break;
		}
	}

	return false;
}





bool Input::isKeyDown(SDL_Keycode key)
{
	return keysDown[key];
}

bool Input::isKeyUp(SDL_Keycode key)
{
	return keysUp[key];
}

bool Input::isKeyHeld(SDL_Keycode key)
{
	return keysHeld[key];
}





bool Input::isButtonDown(uint8_t button)
{
	return buttonsDown[button];
}

bool Input::isButtonUp(uint8_t button)
{
	return buttonsUp[button];
}

bool Input::isButtonHeld(uint8_t button)
{
	return buttonsHeld[button];
}





Vector2 Input::getMousePos()
{
	return mousePos;
}
