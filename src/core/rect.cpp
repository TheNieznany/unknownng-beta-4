/*
 * My 2D game engine written in C++ and SDL2
 * Copyright (C) 2022  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This file is part of unknownNG Beta 4.
 *
 * unknownNG Beta 4 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * unknownNG Beta 4 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with unknownNG Beta 4.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "core/rect.hpp"

Rect::Rect()
{
	x = 0.0;
	y = 0.0;
	w = 0.0;
	h = 0.0;
}

Rect::Rect(double a_x, double a_y, double a_w, double a_h)
{
	x = a_x;
	y = a_y;
	w = a_w;
	h = a_h;
}

Rect::Rect(const Rect& r)
{
	x = r.x;
	y = r.y;
	w = r.w;
	h = r.h;
}

Rect& Rect::operator=(const Rect& rhs)
{
	if (this != &rhs)
	{
		x = rhs.x;
		y = rhs.y;
		w = rhs.w;
		h = rhs.h;
	}

	return *this;
}





bool Rect::collidesWith(const Rect& r) const
{
	return (
		x < (r.x + r.w)	&&
		y < (r.y + r.h)	&&
		(x + w) > r.x	&&
		(y + h) > r.y
	);
}

bool Rect::hasPoint(const Vector2& p) const
{
	return (
		x < p.x			&&
		y < p.y			&&
		(x + w) > p.x	&&
		(y + h) > p.y
	);
}
