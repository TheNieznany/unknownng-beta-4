#
# My 2D game engine written in C++ and SDL2
# Copyright (C) 2022  TheNieznany <thenieznany11 at protonmail dot com>
#
# This file is part of unknownNG Beta 4.
#
# unknownNG Beta 4 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# unknownNG Beta 4 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with unknownNG Beta 4.  If not, see <https://www.gnu.org/licenses/>.
#

CXX = g++

CXXFLAGS := -fmax-errors=4 -fno-common -fstack-usage
CXXFLAGS := $(CXXFLAGS) -ggdb -g3 -Og
CXXFLAGS := $(CXXFLAGS) -Wall -Wconversion -Wdouble-promotion -Wextra -Wformat=2
CXXFLAGS := $(CXXFLAGS) -Wformat-overflow -Wformat-truncation -Wshadow
CXXFLAGS := $(CXXFLAGS) -Wstack-usage=8192 -Wundef

LDFLAGS = -lSDL2main -lSDL2 -lSDL2_image

SRCDIR = src
SRCDIR_CORE = src/core

INCDIR = include
INCDIR_CORE = include/core

OBJDIR = build
OBJDIR_CORE = build/core





sources = $(shell find $(SRCDIR) -name "*.cpp")
sources_core = $(shell find $(SRCDIR_CORE) -name "*.cpp")

objects = $(sources:$(SRCDIR)/%.cpp=$(OBJDIR)/%.o)
objects_core = $(sources_core:$(SRCDIR_CORE)/%.cpp=$(OBJDIR_CORE)/%.o)



test: $(objects) $(objects_core)
	$(CXX) $^ -o $@ $(LDFLAGS)

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp $(INCDIR)/%.hpp
	$(CXX) $(CXXFLAGS) -I $(INCDIR) -c $< -o $@

$(OBJDIR_CORE)/%.o: $(SRCDIR_CORE)/%.cpp $(INCDIR_CORE)/%.hpp
	$(CXX) $(CXXFLAGS) -I $(INCDIR) -c $< -o $@

clean:
	$(RM) $(objects) $(objects_core) $(OBJDIR)/*.su $(OBJDIR_CORE)/*.su
